# Addon IDE (Android.V6)

> [!CAUTION]
> Addon IDE现因诸多原因停更并解散，其服务不再支持，祝后会有期。

第六代：针对Minecraft基岩版设计的一款模组制作器软件。

页面简洁、无广告。功能丰富、全面，支持检查调试，适用于新手和有经验的开发者，轻松创造个性化游戏体验！软件不含付费功能。

下载 `.apk` 文件：[123云盘](https://www.123pan.com/s/vhjA-gqXsH.html)

下载 `.tsp` 文件：[123云盘](https://www.123pan.com/s/vhjA-SFPsH.html)

---

结绳 开发
